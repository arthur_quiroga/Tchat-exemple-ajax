<?php

ini_set("display_errors", 1);

function addMsg($msg, $author, $date, &$json_array) {
    // Je cree le nouveau message au format JSON
    $new_msg = '{"data-id":'.count($json_array).', "author":"'.$author.'", "date":"'.$date.'", "msg":"'.$msg.'"}';
    $new_msg = [
        "data-id" => count($json_array),
        "author" => $author,
        "date" => $date,
        "msg" => $msg
    ];
    // J'ajoute mon message en format json dans mon tableau json
    array_push($json_array, $new_msg);
}

function getMsg($lastMessage, $json_array) {
    $result = [];
    // Je recupere les derniers messages depuis $lastMessage
    for ($k = $lastMessage + 1; $k < count($json_array); ++$k) {
        array_push($result, $json_array[$k]);
    }
    return $result;
}

// Je lis mon fichier json et je transforme le resultat de la lecture (string) en json grace a json_decode
$msg_json = json_decode(file_get_contents('msg.json'));

// En fonction de op, j'ajoute un message
if ($_POST['op'] == "addMsg") {
     addMsg($_POST['msg'], $_POST['author'], $_POST['date'], $msg_json);    // & pour un passage par reference
     //j'ecris tous les messages dans le fichier (le reste est ecrase)
     file_put_contents('msg.json', json_encode($msg_json));
}

// je recupere les derniers message et je les mets en version string.
// Et je fais un ECHO de ca pour pouvoir le recuperer dans la fontion ajax
echo json_encode(getMsg($_POST['lastMsg'], $msg_json));


?>
